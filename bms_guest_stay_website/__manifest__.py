# -*- coding: utf-8 -*-
{
    'name': 'BMS Guest Stay Website Management',
    'version': '10.0.1.0.0',
    'author': 'BMS Group Global',
    'license': 'AGPL-3',
    'summary': 'BMS guest stay website',
    'sequence': 32,
    'category': 'BMS Module',
    'website': 'http://bmsgroupglobal.com/',
    'images': [],
    'depends': ['bms_guest_stay',
                'website'],
    'data': [
        'template/homepage.xml',
        'template/signup.xml',
    ],
    'demo': [
    ],
    'qweb': [
    ],
    'css': [
        'static/src/css/fonts.css',
        'static/src/css/style.css',
        'static/src/css/signup.css',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}
