# -*- coding: utf-8 -*-
from odoo import fields, models, api


class ResPartner(models.Model):
    _inherit = 'res.partner'

    identity_card = fields.Char()
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    sex = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female')
    ], string='Gender')
    birthday = fields.Date()
    is_group = fields.Boolean(compute='_compute_is_group',
                              search='_search_is_group', store=True)
    is_single_customer = fields.Boolean(compute='_compute_is_single_customer',
                                        search='_search_is_single_customer',
                                        store=True)

    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      search='_search_is_is_super_manager',
                                      store=True)
    is_management_unit = fields.Boolean(compute='_compute_is_management_unit',
                                        search='_search_is_management_unit',
                                        store=True)
    is_accommodation = fields.Boolean(compute='_compute_is_accommodation',
                                      search='_search_is_accommodation',
                                      store=True)

    # các cơ sở lưu trú của partner của tk ĐVCQ
    accommodation_ids = fields.One2many('bms.accommodation', 'partner_id',
                                        string='accommodation', readonly=True)

    # các cơ sở lưu trú khách hàng từng ở
    customer_accommodation_ids = fields.Many2many('bms.accommodation',
                                                  'bms_guest_stay',
                                                  'partner_id',
                                                  'accommodation_id')

    @api.model
    def default_get(self, fields):
        res = super(ResPartner, self).default_get(fields)
        ctx = self.env.context or {}
        if ctx.get('default_is_group', False):
            group_tag_id = self.env.ref(
                'bms_guest_stay.res_partner_category_group_customer').id
            res['category_id'] = [(6, False, [group_tag_id])]
            res['is_group'] = True
        return res

    @api.model
    def create(self, vals):
        ctx = self.env.context or {}
        if ctx.get('default_is_group', False):
            vals['is_group'] = True
        return super(ResPartner, self).create(vals)

    @api.multi
    def _search_is_group(self, operator, value):
        group_tag_id = self.env.ref(
            'bms_guest_stay.res_partner_category_group_customer').id
        if value:
            return [('category_id', 'in', group_tag_id)]
        return [('category_id', 'not in', group_tag_id)]

    @api.depends('category_id')
    @api.multi
    def _compute_is_group(self):
        group_tag = self.env.ref(
            'bms_guest_stay.res_partner_category_group_customer')
        for p in self:
            p.is_group = False
            if group_tag in p.category_id:
                p.is_group = True

    @api.multi
    def _search_is_single_customer(self, operator, value):
        single_customer_tag_id = self.env.ref(
            'bms_guest_stay.res_partner_category_single_customer').id
        if value:
            return [('category_id', 'in', single_customer_tag_id)]
        return [('category_id', 'not in', single_customer_tag_id)]

    @api.depends('category_id')
    @api.multi
    def _compute_is_single_customer(self):
        single_customer_tag = self.env.ref(
            'bms_guest_stay.res_partner_category_single_customer')
        for p in self:
            p.is_single_customer = False
            if single_customer_tag in p.category_id:
                p.is_single_customer = True

    @api.multi
    def _search_is_super_manager(self, operator, value):
        super_manager_tag_id = self.env.ref(
            'bms_guest_stay.res_partner_category_super_manager').id
        if value:
            return [('category_id', 'in', super_manager_tag_id)]
        return [('category_id', 'not in', super_manager_tag_id)]

    @api.depends('category_id')
    @api.multi
    def _compute_is_super_manager(self):
        super_manager_tag = self.env.ref(
            'bms_guest_stay.res_partner_category_super_manager')
        for p in self:
            p.is_super_manager = False
            if super_manager_tag in p.category_id:
                p.is_super_manager = True

    @api.multi
    def _search_is_management_unit(self, operator, value):
        management_unit_id = self.env.ref(
            'bms_guest_stay.res_partner_category_management_unit').id
        if value:
            return [('category_id', 'in', management_unit_id)]
        return [('category_id', 'not in', management_unit_id)]

    @api.depends('category_id')
    @api.multi
    def _compute_is_management_unit(self):
        management_unit_tag = self.env.ref(
            'bms_guest_stay.res_partner_category_management_unit')
        for p in self:
            p.is_management_unit = False
            if management_unit_tag in p.category_id:
                p.is_management_unit = True

    @api.multi
    def _search_is_accommodation(self, operator, value):
        accommodation_id = self.env.ref(
            'bms_guest_stay.res_partner_category_accommodation').id
        if value:
            return [('category_id', 'in', accommodation_id)]
        return [('category_id', 'not in', accommodation_id)]

    @api.depends('category_id')
    @api.multi
    def _compute_is_accommodation(self):
        accommodation_tag = self.env.ref(
            'bms_guest_stay.res_partner_category_accommodation')
        for p in self:
            p.is_accommodation = False
            if accommodation_tag in p.category_id:
                p.is_accommodation = True

    @api.model
    def create(self, vals):
        single_customer_tag = self.env.ref(
            'bms_guest_stay.res_partner_category_single_customer').id
        if vals.get('category_id', False):
            for item in vals['category_id']:
                if item[0] == 4:
                    if item[1] == single_customer_tag:
                        vals['is_single_customer'] = True
                        continue
                if item[0] == 6:
                    if single_customer_tag in item[2]:
                        vals['is_single_customer'] = True
        return super(ResPartner, self).create(vals)
