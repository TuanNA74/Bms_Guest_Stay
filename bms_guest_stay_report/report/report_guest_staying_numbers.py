# -*- coding: utf-8 -*-
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
from odoo import fields, models, tools


class ReportGuestStayingNumber(models.Model):
    _name = 'report.guest.staying.numbers'
    _auto = False

    name = fields.Char()
    accommodation_id = fields.Many2one('bms.accommodation',
                                       string='Accommodation')
    # accommodation_name = fields.Char()
    partner_id = fields.Many2one('res.partner',
                                 string='Management unit')
    phone = fields.Char()
    street = fields.Char()
    state_id = fields.Many2one('res.country.state',
                               string='State')
    # state_name = fields.Char()
    district_id = fields.Many2one('res.country.district',
                                  string='District')
    district_name = fields.Char()
    ward_id = fields.Many2one('res.country.ward',
                              string='Ward')
    # ward_name = fields.Char()
    accommodation_address = fields.Char()
    day_number = fields.Integer()
    day_staying = fields.Integer()

    def init(self):
        tools.drop_view_if_exists(self._cr, self._table)
        self._cr.execute(
            """CREATE or REPLACE view report_guest_staying_numbers as (
                select
                    g.id as id,
                    g.name as name,
                    g.day_number as day_number,
                    g.from_date,
                    now()::date -from_date::date + 1 as day_staying,
                    rs.id as state_id,
                    rd.id as district_id,
                    rw.id as ward_id,
                    coalesce(a.street || ', ' , '') ||
                    coalesce(arw.name || ', ' , '') ||
                    coalesce(ard.name|| ', ', '')||
                    coalesce(ars.name, '') as accommodation_address,
                    a.partner_id as partner_id,
                    a.id as accommodation_id,
                    a.mobile as phone
                from bms_guest_stay as g
                    left join bms_accommodation
                    as a on g.accommodation_id = a.id
                    left join res_partner
                    as p on p.id = a.partner_id
                    left join res_country
                    as rc on rc.id = p.country_id
                    left join res_country_state
                    as rs on rs.id = p.state_id
                    left join res_country_district
                    as rd on rd.id = p.district_id
                    left join res_country_ward
                    as rw on rw.id = p.ward_id
                    left join res_country_state
                    as ars on ars.id = a.state_id
                    left join res_country_district
                    as ard on ard.id = a.district_id
                    left join res_country_ward
                    as arw on arw.id = a.ward_id
                    where g.day_number != -1
                    )
            """
        )


class ReportGuestStayingNumbersXlsx(ReportXlsx):
    def generate_xlsx_report(self, workbook, data, wizards):
        report_name = str(wizards.id)
        sheet = workbook.add_worksheet(report_name[:31])

        header_bold = workbook.add_format({'bold': True})
        header_bold.set_text_wrap()
        header_bold.set_align('center')
        header_bold.set_font_name('Times New Roman')
        header_bold.set_bottom()
        header_bold.set_top()
        header_bold.set_left()
        header_bold.set_right()
        header_bold.set_font_size(9)
        header_bold.set_fg_color('green')

        bold = workbook.add_format({'bold': True})
        bold.set_text_wrap()
        bold.set_align('center')
        bold.set_font_name('Times New Roman')
        bold.set_font_size(11)

        border_bold = workbook.add_format({'bold': True})
        border_bold.set_text_wrap()
        border_bold.set_align('center')
        border_bold.set_font_name('Times New Roman')
        border_bold.set_bottom()
        border_bold.set_top()
        border_bold.set_left()
        border_bold.set_right()
        border_bold.set_fg_color('gray')

        red_bold_left = workbook.add_format({'bold': True})
        red_bold_left.set_text_wrap()
        red_bold_left.set_align('left')
        red_bold_left.set_font_name('Times New Roman')
        red_bold_left.set_bottom()
        red_bold_left.set_top()
        red_bold_left.set_left()
        red_bold_left.set_right()
        red_bold_left.set_fg_color('pink')

        orange_bold_center = workbook.add_format({'bold': True})
        orange_bold_center.set_text_wrap()
        orange_bold_center.set_align('center')
        orange_bold_center.set_font_name('Times New Roman')
        orange_bold_center.set_bottom()
        orange_bold_center.set_top()
        orange_bold_center.set_left()
        orange_bold_center.set_right()
        orange_bold_center.set_fg_color('yellow')

        orange_bold_left = workbook.add_format({'bold': True})
        orange_bold_left.set_text_wrap()
        orange_bold_left.set_align('left')
        orange_bold_left.set_font_name('Times New Roman')
        orange_bold_left.set_bottom()
        orange_bold_left.set_top()
        orange_bold_left.set_left()
        orange_bold_left.set_right()
        orange_bold_left.set_fg_color('yellow')

        normal = workbook.add_format({})
        normal.set_text_wrap()
        normal.set_align('left')
        normal.set_font_name('Times New Roman')
        normal.set_bottom()
        normal.set_top()
        normal.set_left()
        normal.set_right()
        normal.set_align('vjustify')

        normal_normal = workbook.add_format({})
        normal_normal.set_font_name('Times New Roman')
        normal_normal.set_text_wrap()
        normal_normal.set_align('center')
        normal_normal.set_bottom()
        normal_normal.set_top()
        normal_normal.set_left()
        normal_normal.set_right()

        normal_normal_note = workbook.add_format({})
        normal_normal_note.set_font_name('Times New Roman')
        normal_normal_note.set_font_size(9)

        bold_1 = workbook.add_format({'bold': True})
        bold_1.set_font_name('Times New Roman')
        bold_1.set_font_size(9)

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 40)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 20)

        row_pos = 1
        sheet.merge_range('A{}:B{}'.format(row_pos, row_pos), u'BỘ CÔNG AN',
                          bold)
        sheet.merge_range('C{}:D{}'.format(row_pos, row_pos + 1),
                          u'CÔNG HÒA XÃ HỘI CHỦ NGHĨA VIÊT NAM \n'
                          u'Độc lập - Tự do - Hạnh phúc', bold)
        row_pos += 3
        sheet.merge_range('D{}:E{}'.format(row_pos, row_pos),
                          u'..........., ngày .....tháng ....... năm ...…',
                          bold)
        row_pos += 2
        day_staying_label = u'BÁO CÁO KHÁCH ĐANG LƯU TRÚ TRÊN {} NGÀY'\
            .format(wizards.day_staying)
        sheet.merge_range('A{}:E{}'.format(row_pos, row_pos),
                          day_staying_label,
                          header_bold)

        row_pos += 2
        sheet.merge_range('A{}:A{}'.format(row_pos, row_pos + 1),
                          u'STT', header_bold)
        sheet.merge_range('B{}:B{}'.format(row_pos, row_pos + 1),
                          u'Tên Cơ Sở Lưu Trú', header_bold)
        sheet.merge_range('C{}:C{}'.format(row_pos, row_pos + 1),
                          u'Địa Chỉ', header_bold)
        sheet.merge_range('D{}:D{}'.format(row_pos, row_pos + 1),
                          u'Điện Thoại', header_bold)
        sheet.merge_range('E{}:E{}'.format(row_pos, row_pos + 1),
                          u'Số Khách', header_bold)

        row_pos += 1
        for i in range(0, 5):
            sheet.write(row_pos, i, '({})'.format(i + 1), orange_bold_center)

        sheet.freeze_panes(10, 5)

        row_pos += 1
        datas = wizards.get_datas()
        count_index = 0

        for data in datas:
            count_index += 1
            sheet.write(row_pos, 0, count_index or '', normal)
            sheet.write(row_pos, 1, data['accommodation_name'] or '', normal)
            sheet.write(row_pos, 2,
                        data['accommodation_address'] or '', normal)
            sheet.write(row_pos, 3, data['phone'] or '', normal_normal)
            sheet.write(row_pos, 4,
                        data['number_staying'] or '', normal_normal)
            row_pos += 1

        row_pos += 2
        sheet.write(row_pos, 1, u'NGƯỜI BÁO CÁO', bold)
        row_pos += 1
        sheet.merge_range('D{}:E{}'.format(row_pos, row_pos),
                          u'THỦ TRƯỞNG ĐƠN VỊ', bold)
        row_pos += 1
        sheet.merge_range('D{}:E{}'.format(row_pos, row_pos),
                          u'', bold)


ReportGuestStayingNumbersXlsx(
    'report.bms_guest_stay.guest_staying_numbers_xls.xlsx',
    'wizard.report.guest.staying.numbers')
