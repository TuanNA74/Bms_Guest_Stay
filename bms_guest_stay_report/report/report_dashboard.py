# -*- coding: utf-8 -*-
from odoo import fields, models, api, _


class ReportDashBoard(models.Model):
    _name = 'report.dash.board'

    total_guest_by_type = fields.Integer(compute='_compute_guest_by_type')
    guest_type = fields.Selection([('all', 'All Guest Staying'),
                                   ('foreign', 'Foreign Staying'),
                                   ('overseas_vietnamese',
                                    'Overseas Vietnamese Staying'),
                                   ('vietnamese', 'Vietnamese Staying')])

    @api.depends('guest_type')
    @api.model
    def _compute_guest_by_type(self):
        context = self.env.context or {}
        domain = context.get('domain', [])
        vn_country_id = self.env.ref('base.vn').id

        domain_vietnamese = [
            ('day_state', '!=', _('Check Out')),
            ('overseas_vietnamese', '!=', True),
            ('country_id', '=', vn_country_id)
        ]
        domain_vietnamese.extend(domain)

        domain_overseas_vietnamese = [
            ('day_state', '!=', _('Check Out')),
            ('overseas_vietnamese', '=', True)
        ]
        domain_overseas_vietnamese.extend(domain)

        domain_foreign = [
            ('day_state', '!=', _('Check Out')),
            ('country_id', '!=', vn_country_id),
            ('overseas_vietnamese', '!=', True)
        ]
        domain_foreign.extend(domain)

        for s in self:
            if s.guest_type == 'vietnamese':
                total_guest_vietnamese = s.env['bms.guest.stay'].\
                    search_count(domain_vietnamese)
                s.total_guest_by_type = total_guest_vietnamese

            elif s.guest_type == 'overseas_vietnamese':
                total_guest_overseas_vietnamese = s.env['bms.guest.stay'].\
                    search_count(domain_overseas_vietnamese)
                s.total_guest_by_type = total_guest_overseas_vietnamese

            elif s.guest_type == 'foreign':
                total_guest_foreign = s.env['bms.guest.stay'].\
                    search_count(domain_foreign)
                s.total_guest_by_type = total_guest_foreign

            else:
                total_guest_vietnamese = s.env['bms.guest.stay'].\
                    search_count(domain_vietnamese)

                total_guest_overseas_vietnamese = s.env['bms.guest.stay'].\
                    search_count(domain_overseas_vietnamese)
                total_guest_foreign = s.env['bms.guest.stay'].\
                    search_count(domain_foreign)

                s.total_guest_by_type = \
                    total_guest_vietnamese \
                    + total_guest_overseas_vietnamese + total_guest_foreign

    @api.multi
    def action_view_guest(self):
        context = self.env.context or {}
        domain = context.get('domain', [])
        vn_country_id = self.env.ref('base.vn').id
        domain_dict = {
            'vietnamese': [('day_state', '!=', _('Check Out')),
                           ('overseas_vietnamese', '!=', True),
                           ('country_id', '=', vn_country_id)],
            'overseas_vietnamese': [('day_state', '!=', _('Check Out')),
                                    ('overseas_vietnamese', '=', True)],
            'foreign': [('day_state', '!=', _('Check Out')),
                        ('country_id', '!=', vn_country_id),
                        ('overseas_vietnamese', '!=', True)],
            'all': [('day_state', '!=', _('Check Out'))]
        }
        action = self.env.ref('bms_guest_stay.action_bms_guest_stay')
        res = action.read()[0]
        domain_view = domain_dict[self[0].guest_type]
        domain_view.extend(domain)
        res['domain'] = domain_view
        return res
