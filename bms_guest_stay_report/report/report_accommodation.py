# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.addons.report_xlsx.report.report_xlsx import ReportXlsx
from odoo import tools
from datetime import datetime


class ReportAccommodation(models.Model):
    _name = 'report.accommodation'
    _auto = False

    name = fields.Char()
    contact_id = fields.Many2one('res.partner', string='Contact')
    contact_name = fields.Char(string='Owner/Representative')
    birthday = fields.Date()
    sex = fields.Selection([
        ('male', 'Male'),
        ('female', 'Female')
    ], string='Gender')
    phone = fields.Char()
    identity_card = fields.Char()
    issued_date = fields.Date()
    issued_by = fields.Char()
    street = fields.Char()
    ward_id = fields.Many2one('res.country.ward', string='Ward')
    district_id = fields.Many2one('res.country.district', string='District')
    state_id = fields.Many2one('res.country.state', string='State')
    # country_id = fields.Many2one('res.country', string='Country')
    accommodation_street = fields.Char(compute='_compute_accommodation_street')
    onebed_no = fields.Integer(string='Room with 1 bed')
    twobed_no = fields.Integer(string='Room with 2 bed')
    threebed_no = fields.Integer(string='Room with 3 bed')
    fourbed_no = fields.Integer(string='Room with 4 bed')
    totalbed_no = fields.Integer(string='Total room',
                                 compute='_compute_totalbed_no')
    note = fields.Text()
    partner_id = fields.Many2one('res.partner', string='Partner')
    partner_name = fields.Char(string='Management Unit')

    @api.multi
    def _compute_accommodation_street(self):
        for report in self:
            accommodation_street = \
                u'{}-{}-{}'.format(
                    report.ward_id.name or '',
                    report.district_id.name or '',
                    report.state_id.name or '',
                )
            accommodation_street = accommodation_street.replace('--', '-')
            if accommodation_street[0] == '-':
                accommodation_street = accommodation_street[1:]
            report.accommodation_street = accommodation_street

    @api.multi
    def _compute_totalbed_no(self):
        for accommodation in self:
            accommodation.totalbed_no = \
                accommodation.onebed_no + accommodation.twobed_no \
                + accommodation.threebed_no + accommodation.fourbed_no

    def init(self):
        tools.drop_view_if_exists(self._cr, self._table)
        self._cr.execute("""
            CREATE or REPLACE view report_accommodation as (
select
a.id as id
,a.name as name
,a.contact_id as contact_id
,rp.name as contact_name
,rp.sex as sex
,rp.birthday as birthday
,a.phone as phone
,a.identity_card as identity_card
,issued_date as issued_date
,issued_by as issued_by
,a.onebed_no as onebed_no
,a.twobed_no as twobed_no
,a.threebed_no as threebed_no
,a.fourbed_no as fourbed_no
,a.note as note
,a.street as street
,a.state_id as state_id
,a.district_id as district_id
,a.ward_id as ward_id
,a.partner_id as partner_id
,rp1.name as partner_name
from bms_accommodation as a
left join res_partner as rp on rp.id = a.contact_id
left join res_partner as rp1 on rp1.id = a.partner_id
order by id desc
            )
        """)


class ReportAccommodationXlsx(ReportXlsx):
    def generate_xlsx_report(self, workbook, data, wizard):
        sheet = workbook.add_worksheet('ThongkeCSLT')

        header_bold = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        header_bold.set_text_wrap()
        header_bold.set_align('center')
        header_bold.set_font_name('Times New Roman')
        header_bold.set_bottom()
        header_bold.set_top()
        header_bold.set_left()
        header_bold.set_right()
        header_bold.set_font_size(17)
        header_bold.set_fg_color('#bcdb0f')
        header_bold.set_font_color('blue')

        bold = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        bold.set_text_wrap()
        bold.set_align('center')
        bold.set_font_name('Times New Roman')
        bold.set_font_size(11)

        border_bold = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        border_bold.set_text_wrap()
        border_bold.set_align('center')
        border_bold.set_font_name('Times New Roman')
        border_bold.set_bottom()
        border_bold.set_top()
        border_bold.set_left()
        border_bold.set_right()
        border_bold.set_fg_color('#efad07')
        border_bold.set_font_color('blue')

        red_bold_left = workbook.add_format(
            {'bold': True, 'valign': 'vcenter'})
        red_bold_left.set_text_wrap()
        red_bold_left.set_align('left')
        red_bold_left.set_font_name('Times New Roman')
        red_bold_left.set_bottom()
        red_bold_left.set_top()
        red_bold_left.set_left()
        red_bold_left.set_right()
        red_bold_left.set_fg_color('pink')

        orange_bold_left = workbook.add_format(
            {'bold': True, 'valign': 'vcenter'})
        orange_bold_left.set_text_wrap()
        orange_bold_left.set_align('left')
        orange_bold_left.set_font_name('Times New Roman')
        orange_bold_left.set_bottom()
        orange_bold_left.set_top()
        orange_bold_left.set_left()
        orange_bold_left.set_right()
        orange_bold_left.set_fg_color('yellow')

        normal = workbook.add_format({'valign': 'vcenter'})
        normal.set_text_wrap()
        normal.set_align('center')
        normal.set_font_name('Times New Roman')

        normal_normal = workbook.add_format({'valign': 'vcenter'})
        normal_normal.set_font_name('Times New Roman')
        normal_normal.set_text_wrap()
        normal_normal.set_top()
        normal_normal.set_bottom()
        normal_normal.set_left()
        normal_normal.set_right()

        normal_normal_note = workbook.add_format({'valign': 'vcenter'})
        normal_normal_note.set_font_name('Times New Roman')
        normal_normal_note.set_font_size(9)

        bold_1 = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        bold_1.set_font_name('Times New Roman')
        bold_1.set_font_size(11)

        bold_2 = workbook.add_format({'bold': True, 'valign': 'vcenter'})
        bold_2.set_font_name('Times New Roman')
        bold_2.set_font_size(11)
        bold_2.set_font_color('blue')

        sheet.set_default_row(25)
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 25)
        sheet.set_column('C:C', 25)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 16)
        sheet.set_column('G:G', 12)
        sheet.set_column('H:H', 12)
        sheet.set_column('I:I', 25)
        sheet.set_column('J:J', 36)
        sheet.set_column('K:K', 16)
        sheet.set_column('L:L', 10)
        sheet.set_column('M:M', 10)
        sheet.set_column('N:N', 10)
        sheet.set_column('O:O', 10)
        sheet.set_column('P:P', 15)

        datas = wizard.get_datas()
        day = datetime.utcnow().day
        month = datetime.utcnow().month
        year = datetime.utcnow().year
        row_pos = 1
        sheet.merge_range('A{}:C{}'.format(row_pos, row_pos),
                          u'Đơn vị: Tai khoan thu nghiem', bold_1)
        sheet.merge_range('E{}:I{}'.format(row_pos, row_pos),
                          u'CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM', bold)

        row_pos += 1
        sheet.merge_range('A{}:C{}'.format(row_pos, row_pos),
                          u'Địa chỉ:', bold_1)
        sheet.merge_range('E{}:I{}'.format(row_pos, row_pos),
                          u'Độc lập - Tự do - Hạnh phúc', bold)
        date = u'Ngày {} tháng {} năm {}'.format(day, month, year)
        sheet.write(row_pos, 8, date, normal)

        row_pos += 3
        sheet.merge_range('A{}:I{}'.format(row_pos, row_pos),
                          u'THỐNG KÊ CƠ SỞ LƯU TRÚ',
                          header_bold)

        row_pos += 1
        sheet.merge_range('B{}:G{}'.format(row_pos, row_pos),
                          u'Kính gửi: …………………………………………………………………………………………………',
                          bold_2)

        row_pos += 1
        sheet.merge_range('A{}:A{}'.format(row_pos, row_pos + 1), u'STT',
                          border_bold)
        sheet.merge_range('B{}:B{}'.format(row_pos, row_pos + 1),
                          u'TÊN CỚ SỞ LƯU TRÚ', border_bold)
        sheet.merge_range('C{}:C{}'.format(row_pos, row_pos + 1),
                          u'CHỦ SỞ HỮU / NGƯỜI ĐẠI DIỆN THEO PHÁP LUẬT',
                          border_bold)
        sheet.merge_range('D{}:E{}'.format(row_pos, row_pos), u'NĂM SINH',
                          border_bold)
        sheet.write(row_pos, 3, u'NAM', border_bold)
        sheet.write(row_pos, 4, u'NỮ', border_bold)
        sheet.merge_range('F{}:F{}'.format(row_pos, row_pos + 1),
                          u'ĐIỆN THOẠI LIÊN HỆ LỄ TÂN',
                          border_bold)
        sheet.merge_range('G{}:I{}'.format(row_pos, row_pos),
                          u'SỐ CMND/HỘ CHIẾU CHỦ SỞ HỮU', border_bold)
        sheet.write(row_pos, 6, u'Số', border_bold)
        sheet.write(row_pos, 7, u'Ngày cấp', border_bold)
        sheet.write(row_pos, 8, u'Nơi cấp', border_bold)
        sheet.merge_range('J{}:J{}'.format(row_pos, row_pos + 1),
                          u'ĐỊA CHỈ KINH DOANH', border_bold)
        sheet.merge_range('K{}:K{}'.format(row_pos, row_pos + 1),
                          u'TỔNG SỐ PHÒNG', border_bold)
        sheet.merge_range('L{}:O{}'.format(row_pos, row_pos),
                          u'TỔNG SỐ GIƯỜNG/PHÒNG', border_bold)
        sheet.write(row_pos, 11, u'1\nGiường', border_bold)
        sheet.write(row_pos, 12, u'2\nGiường', border_bold)
        sheet.write(row_pos, 13, u'3\nGiường', border_bold)
        sheet.write(row_pos, 14, u'4\nGiường', border_bold)
        sheet.merge_range('P{}:P{}'.format(row_pos, row_pos + 1),
                          u'GHI CHÚ', border_bold)

        row_pos += 1
        for i in range(0, 16):
            sheet.write(row_pos, i, '({})'.format(i + 1), border_bold)

        row_pos += 1
        sequence = 1
        for data in datas:
            sheet.write(row_pos, 0, sequence or '', normal_normal)
            sheet.write(row_pos, 1, data.name or '', normal_normal)
            sheet.write(row_pos, 2, data.contact_name or '', normal_normal)
            birthday = data.birthday or ''
            if birthday != '':
                birthday = \
                    birthday[8:] + '/' + birthday[5:7] + '/' + birthday[:4]
            if data.sex == 'male':
                sheet.write(row_pos, 3, birthday or '', normal_normal)
                sheet.write(row_pos, 4, '', normal_normal)
            elif data.sex == 'female':
                sheet.write(row_pos, 3, '', normal_normal)
                sheet.write(row_pos, 4, birthday or '', normal_normal)
            sheet.write(row_pos, 5, data.phone or '', normal_normal)
            sheet.write(row_pos, 6, data.identity_card or '', normal_normal)
            issued_date = data.issued_date or ''
            if issued_date != '':
                issued_date = \
                    issued_date[8:] + '/' + issued_date[5:7] \
                    + '/' + issued_date[:4]
            sheet.write(row_pos, 7, issued_date, normal_normal)
            sheet.write(row_pos, 8, data.issued_by or '', normal_normal)
            sheet.write(row_pos, 9, data.accommodation_street or '',
                        normal_normal)
            sheet.write(row_pos, 10, data.totalbed_no or '', normal_normal)
            sheet.write(row_pos, 11, data.onebed_no or '', normal_normal)
            sheet.write(row_pos, 12, data.twobed_no or '', normal_normal)
            sheet.write(row_pos, 13, data.threebed_no or '', normal_normal)
            sheet.write(row_pos, 14, data.fourbed_no or '', normal_normal)
            sheet.write(row_pos, 15, data.note or '', normal_normal)

            row_pos += 1
            sequence += 1


ReportAccommodationXlsx('report.bms_guest_stay_report.accommodation.xlsx',
                        'wizard.report.accommodation')
