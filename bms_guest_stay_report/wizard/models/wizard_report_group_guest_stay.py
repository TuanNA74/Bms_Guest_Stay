# -*- coding: utf-8 -*-
from odoo import fields, models, api, _, exceptions
import datetime
from dateutil.relativedelta import relativedelta
import pytz
from datetime import timedelta
from odoo.tools.misc import DEFAULT_SERVER_DATE_FORMAT


class WizardReportGuestByDay(models.TransientModel):
    _name = 'wizard.report.group.guest.stay'
    _description = 'Wizard report group guest stay'
    _rec_name = 'id'

    def default_from_date(self):
        date_now = datetime.datetime.now()
        date_now = date_now.strftime('%Y-%m') + '-01'
        return date_now

    def default_to_date(self):
        date_now = datetime.datetime.now() + relativedelta(months=1)
        date_now = date_now.strftime('%Y-%m') + '-01'
        date_now = datetime.datetime.strptime(date_now, '%Y-%m-%d')
        date_now = date_now + relativedelta(days=-1)
        date_now = date_now.strftime('%Y-%m-%d')
        return date_now

    def _state_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return self.env.user.state_id.id
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.state_id.id
        return False

    def _district_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.district_id.id
        return False

    def _ward_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.ward_id.id
        return False

    def _management_unit_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.partner_id.id
        return False

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    state_id = fields.Many2one('res.country.state', string='State',
                               default=_state_id_default)
    district_id = fields.Many2one('res.country.district', string='District',
                                  default=_district_id_default)
    ward_id = fields.Many2one('res.country.ward', string='Ward',
                              default=_ward_id_default)
    management_unit_id = fields.Many2one('res.partner',
                                         string='Management unit',
                                         default=_management_unit_id_default)

    from_date = fields.Date(required=True, default=default_from_date)
    to_date = fields.Date(required=True, default=default_to_date)

    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    @api.multi
    def management_unit_onchange(
            self, state_id, district_id, ward_id, context={}):

        res = {
            'value': {}
        }
        if state_id:
            res['domain'] = {
                'management_unit_id': [('state_id', '=', state_id),
                                       ('is_management_unit', '=', True)]
            }

        if district_id:
            res['domain'] = {
                'management_unit_id':
                    [('district_id', '=', district_id),
                     ('is_management_unit', '=', True)]
            }

        if ward_id:
            res['domain'] = {
                'management_unit_id': [('ward_id', '=', ward_id),
                                       ('is_management_unit', '=', True)]
            }

        if context.get('change_state', False):
            res['value']['district_id'] = False
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_district', False):
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_ward', False):
            res['value']['management_unit_id'] = False

        return res

    @api.multi
    def change_utc_to_local_datetime(self, souce_date):
        user_tz = self.env.user.tz or str(pytz.utc)
        tz_now = datetime.datetime.now(pytz.timezone(user_tz))
        difference = tz_now.utcoffset().total_seconds() / 60 / 60
        difference = int(difference)
        utc_date = datetime.datetime.strptime(souce_date,
                                              '%Y-%m-%d %H:%M:%S')
        utc_date = utc_date + timedelta(hours=-difference)
        return utc_date.strftime('%Y-%m-%d %H:%M:%S')

    @api.multi
    def export_report(self):
        datas = {'ids': self.ids}
        datas['model'] = 'wizard.report.group.guest.stay'
        datas['form'] = self.read()[0]

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'bms_guest_stay.group_guest_stay_xls.xlsx',
            'datas': datas,
            'name': _('Group guest stay')
        }

    @api.multi
    def get_domain(self):
        domain = []
        state_id = self.state_id.id
        district_id = self.district_id.id
        ward_id = self.ward_id.id
        management_unit_id = self.management_unit_id.id
        from_date = self.from_date + ' 00:00:00'
        from_date = self.change_utc_to_local_datetime(from_date)
        to_date = self.to_date + ' 23:59:59'
        to_date = self.change_utc_to_local_datetime(to_date)
        if state_id:
            domain.append(
                ('state_id', '=', state_id)
            )
        if district_id:
            domain.append(
                ('district_id', '=', district_id)
            )
        if ward_id:
            domain.append(
                ('ward_id', '=', ward_id)
            )
        if management_unit_id:
            domain.append(
                ('partner_id', '=', management_unit_id)
            )

        domain.append(
            ('from_date', '>=', from_date),
        )
        # domain.append('|')
        # domain.append('&')

        domain.append(
            ('from_date', '<=', to_date),
        )
        # domain.append(
        #     ('to_date', '>=', from_date),
        # )
        # domain.append(
        #     ('to_date', '<=', to_date),
        # )
        return domain

    @api.multi
    def get_querry(self):
        state_id = self.state_id.id
        district_id = self.district_id.id
        ward_id = self.ward_id.id
        management_unit_id = self.management_unit_id.id
        from_date = self.from_date + ' 00:00:00'
        from_date = self.change_utc_to_local_datetime(from_date)
        to_date = self.to_date + ' 23:59:59'
        to_date = self.change_utc_to_local_datetime(to_date)
        where_clause = """
                            r.from_date >= '{}'
                            and r.from_date <= '{}'
                        """.format(from_date, to_date)
        if state_id:
            where_clause = \
                where_clause + """ and r.state_id = {}""".format(state_id)

        if district_id:
            where_clause = \
                where_clause + """ and r.district_id = {}
                """.format(district_id)

        if ward_id:
            where_clause = where_clause + """
             and r.ward_id = {}""".format(ward_id)

        if management_unit_id:
            where_clause = where_clause + """ and r.partner_id = {}
            """.format(management_unit_id)
        querry = """
        select rp.name as group_name,
        a.name as accommodation_name,
        count(r.id)
        from report_group_guest_stay as r
          left join res_partner as rp on rp.id = r.group_id
          left join bms_accommodation as a on a.id = r.accommodation_id
        where {}
        group by rp.name,
        a.name
        """.format(where_clause)

        return querry

    @api.multi
    def get_data(self):
        querry = self.get_querry()
        self.env.cr.execute(querry)
        report_s = []
        sum = 0
        for row in self.env.cr.fetchall():
            count = row[2] or 0
            report_s.append({
                'group_name': row[0],
                'accommodation_name': row[1],
                'count': count,
            })
            sum += count
        return report_s, sum

    @api.multi
    def view_report(self):
        action = self.env.ref(
            'bms_guest_stay_report.action_report_group_guest_stay_pivot_view')
        action_view = action.read()
        domain = self.get_domain()
        action_view[0]['domain'] = domain
        return action_view[0]

    @api.multi
    @api.constrains('from_date', 'to_date')
    def _check_date(self):
        for wizard in self:
            from_date = wizard.from_date
            from_date = datetime.datetime.strptime(from_date,
                                                   DEFAULT_SERVER_DATE_FORMAT)
            to_date = wizard.to_date
            to_date = datetime.datetime.strptime(to_date,
                                                 DEFAULT_SERVER_DATE_FORMAT)

            if from_date > to_date:
                raise exceptions.ValidationError(_(
                    'From date must be less than or equal to date!'))
        return True
