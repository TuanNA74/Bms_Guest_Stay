# -*- coding: utf-8 -*-
from odoo import fields, models, api, _, exceptions
import datetime
from dateutil.relativedelta import relativedelta
from odoo.tools.misc import DEFAULT_SERVER_DATE_FORMAT


class WizardReportGuestByCountry(models.TransientModel):
    _name = 'wizard.report.guest.by.country'
    _description = 'Wizard report guest by country'
    _rec_name = 'id'

    def default_from_date(self):
        date_now = datetime.datetime.now()
        date_now = date_now.strftime('%Y-%m') + '-01'
        return date_now

    def default_to_date(self):
        date_now = datetime.datetime.now() + relativedelta(months=1)
        date_now = date_now.strftime('%Y-%m') + '-01'
        date_now = datetime.datetime.strptime(date_now, '%Y-%m-%d')
        date_now = date_now + relativedelta(days=-1)
        date_now = date_now.strftime('%Y-%m-%d')
        return date_now

    def _state_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return self.env.user.state_id.id
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.state_id.id
        return False

    def _district_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.district_id.id
        return False

    def _ward_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.ward_id.id
        return False

    def _management_unit_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.partner_id.id
        return False

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    state_id = fields.Many2one('res.country.state', string='State',
                               default=_state_id_default)
    district_id = fields.Many2one('res.country.district',
                                  string='District',
                                  default=_district_id_default)
    ward_id = fields.Many2one('res.country.ward', string='Ward',
                              default=_ward_id_default)
    management_unit_id = fields.Many2one('res.partner',
                                         string='Management unit',
                                         default=_management_unit_id_default)

    from_date = fields.Date(required=True, default=default_from_date)
    to_date = fields.Date(required=True, default=default_to_date)

    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    @api.multi
    def management_unit_onchange(
            self, state_id, district_id, ward_id, context={}):

        res = {
            'value': {}
        }
        if state_id:
            res['domain'] = {
                'management_unit_id': [('state_id', '=', state_id),
                                       ('is_management_unit', '=', True)]
            }

        if district_id:
            res['domain'] = {
                'management_unit_id':
                    [('district_id', '=', district_id),
                     ('is_management_unit', '=', True)]
            }

        if ward_id:
            res['domain'] = {
                'management_unit_id': [('ward_id', '=', ward_id),
                                       ('is_management_unit', '=', True)]
            }

        if context.get('change_state', False):
            res['value']['district_id'] = False
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_district', False):
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_ward', False):
            res['value']['management_unit_id'] = False

        return res

    def get_querry(self):
        vn_country_id = self.env.ref('base.vn').id
        state_id_where_clause = ""
        if self.state_id:
            state_id_where_clause = """ and state_id ={}""".format(
                self.state_id.id)

        district_id_where_clause = ""
        if self.district_id:
            district_id_where_clause = """ and district_id ={}""".format(
                self.district_id.id)

        ward_id_where_clause = ""
        if self.ward_id:
            ward_id_where_clause = """ and ward_id ={}""".format(
                self.ward_id.id)

        management_unit_id_where_clause = ""
        if self.management_unit_id:
            management_unit_id_where_clause = """ and partner_id ={}""".format(
                self.management_unit_id.id)

        date_where_clause = """from_date >= '{}'
        and from_date <= '{}'""".format(
            self.from_date, self.to_date)
        where_clause = '{} {} {} {} {}'.format(date_where_clause,
                                               state_id_where_clause,
                                               district_id_where_clause,
                                               ward_id_where_clause,
                                               management_unit_id_where_clause)

        count_querry = """
                    select guest_country_id, count(id)
                    from report_guest_by_country
                    where {}
                    group by guest_country_id
                """.format(where_clause)

        male_count_querry = """
                            select guest_country_id, count(id)
                            from report_guest_by_country
                            where {} and gender = 'male'
                            group by guest_country_id
                        """.format(where_clause)

        female_count_querry = """
                                    select guest_country_id, count(id)
                                    from report_guest_by_country
                                    where {} and gender = 'female'
                                    group by guest_country_id
                                """.format(where_clause)

        entry_count_querry = """
                                select guest_country_id, count(id)
                                from report_guest_by_country
                                where {} and guest_country_id != {}
                                group by guest_country_id
                            """.format(where_clause, vn_country_id)

        vn_count_querry = """
                            select guest_country_id, count(id)
                            from report_guest_by_country
                            where {} and guest_country_id = {}
                            group by guest_country_id
                        """.format(where_clause, vn_country_id)

        overseas_vn_count_querry = """
                            select guest_country_id, count(id)
                            from report_guest_by_country
                            where {} and overseas_vietnamese = true
                            group by guest_country_id
                        """.format(where_clause,
                                   vn_country_id)

        return \
            count_querry, \
            female_count_querry, \
            male_count_querry, \
            entry_count_querry, \
            vn_count_querry, \
            overseas_vn_count_querry

    @api.multi
    def get_data(self):
        count_querry, \
            female_count_querry, \
            male_count_querry, \
            entry_count_querry, \
            vn_count_querry, \
            overseas_vn_count_querry = self.get_querry()

        self.env.cr.execute(count_querry)
        count_dict = dict(
            (row[0], row[1] or 0) for row in self.env.cr.fetchall())

        self.env.cr.execute(female_count_querry)
        female_count_dict = dict(
            (row[0], row[1] or 0) for row in self.env.cr.fetchall())

        self.env.cr.execute(male_count_querry)
        male_count_dict = dict(
            (row[0], row[1] or 0) for row in self.env.cr.fetchall())

        self.env.cr.execute(entry_count_querry)
        entry_count_dict = dict(
            (row[0], row[1] or 0) for row in self.env.cr.fetchall())

        self.env.cr.execute(vn_count_querry)
        vn_count_dict = dict(
            (row[0], row[1] or 0) for row in self.env.cr.fetchall())

        self.env.cr.execute(overseas_vn_count_querry)
        overseas_vn_count_dict = dict(
            (row[0], row[1] or 0) for row in self.env.cr.fetchall())

        country_s = self.env['res.country'].browse(count_dict.keys())
        country_dict = dict(
            (country.id, country.name) for country in country_s)

        return \
            count_dict, \
            female_count_dict, \
            male_count_dict, \
            entry_count_dict, \
            vn_count_dict, \
            overseas_vn_count_dict, \
            country_dict

    @api.multi
    def export_report(self):
        datas = {'ids': self.ids}
        datas['model'] = 'wizard.report.guest.by.country'
        datas['form'] = self.read()[0]
        for field in datas['form'].keys():
            if isinstance(datas['form'][field], tuple):
                datas['form'][field] = datas['form'][field][0]

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'bms_guest_stay.guest_by_country_xls.xlsx',
            'datas': datas,
            'name': _('Guest by country')
        }

    @api.multi
    def get_domain(self):
        domain = []
        state_id = self.state_id.id
        district_id = self.district_id.id
        ward_id = self.ward_id.id
        management_unit_id = self.management_unit_id.id
        from_date = self.from_date
        to_date = self.to_date
        if state_id:
            domain.append(
                ('state_id', '=', state_id)
            )
        if district_id:
            domain.append(
                ('district_id', '=', district_id)
            )
        if ward_id:
            domain.append(
                ('ward_id', '=', ward_id)
            )
        if management_unit_id:
            domain.append(
                ('partner_id', '=', management_unit_id)
            )

        domain.append(
            ('from_date', '>=', from_date),
        )
        domain.append(
            ('from_date', '<=', to_date),
        )
        return domain

    @api.multi
    def view_report(self):
        action = self.env.ref(
            'bms_guest_stay_report.action_report_guest_by_country_pivot_view')
        action_view = action.read()
        domain = self.get_domain()
        action_view[0]['domain'] = domain
        return action_view[0]

    @api.multi
    @api.constrains('from_date', 'to_date')
    def _check_date(self):
        for wizard in self:
            from_date = wizard.from_date
            from_date = datetime.datetime.strptime(from_date,
                                                   DEFAULT_SERVER_DATE_FORMAT)
            to_date = wizard.to_date
            to_date = datetime.datetime.strptime(to_date,
                                                 DEFAULT_SERVER_DATE_FORMAT)

            if from_date > to_date:
                raise exceptions.ValidationError(_(
                    'From date must be less than or equal to date!'))
        return True
