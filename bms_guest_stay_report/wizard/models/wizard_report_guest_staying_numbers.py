# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
import datetime
from dateutil.relativedelta import relativedelta
import pytz
from datetime import timedelta


class WizardReportGuestStayingNumbers(models.TransientModel):
    _name = 'wizard.report.guest.staying.numbers'
    _description = 'Wizard Report Guest Staying Numbers'
    _rec_name = 'id'

    def default_from_date(self):
        date_now = datetime.datetime.now()
        date_now = date_now.strftime('%Y-%m') + '-01'
        return date_now

    def default_to_date(self):
        date_now = datetime.datetime.now() + relativedelta(months=1)
        date_now = date_now.strftime('%Y-%m') + '-01'
        date_now = datetime.datetime.strptime(date_now, '%Y-%m-%d')
        date_now = date_now + relativedelta(days=-1)
        date_now = date_now.strftime('%Y-%m-%d')
        return date_now

    @api.multi
    def change_utc_to_local_datetime(self, souce_date):
        user_tz = self.env.user.tz or str(pytz.utc)
        tz_now = datetime.datetime.now(pytz.timezone(user_tz))
        difference = tz_now.utcoffset().total_seconds() / 60 / 60
        difference = int(difference)
        utc_date = datetime.datetime.strptime(souce_date,
                                              '%Y-%m-%d %H:%M:%S')
        utc_date = utc_date + timedelta(hours=-difference)
        return utc_date.strftime('%Y-%m-%d %H:%M:%S')

    def _state_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return self.env.user.state_id.id
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.state_id.id
        return False

    def _district_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.district_id.id
        return False

    def _ward_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.ward_id.id
        return False

    def _management_unit_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.partner_id.id
        return False

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    state_id = fields.Many2one('res.country.state', string='State',
                               required=True, default=_state_id_default)
    district_id = fields.Many2one('res.country.district', string='District',
                                  default=_district_id_default)
    ward_id = fields.Many2one('res.country.ward', string='Ward',
                              default=_ward_id_default)
    management_unit_id = fields.Many2one('res.partner',
                                         string='Management unit',
                                         default=_management_unit_id_default)

    day_staying = fields.Integer()

    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    @api.multi
    def management_unit_onchange(
            self, state_id, district_id, ward_id, context={}):

        res = {
            'value': {}
        }
        if state_id:
            res['domain'] = \
                {'management_unit_id': [('state_id', '=', state_id),
                                        ('is_management_unit', '=', True)]}

        if district_id:
            res['domain'] = \
                {'management_unit_id': [('district_id', '=', district_id),
                                        ('is_management_unit', '=', True)]}

        if ward_id:
            res['domain'] = \
                {'management_unit_id': [('ward_id', '=', ward_id),
                                        ('is_management_unit', '=', True)]}

        if context.get('change_state', False):
            res['value']['district_id'] = False
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_district', False):
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_ward', False):
            res['value']['management_unit_id'] = False

        return res

    # @api.onchange('management_unit_id')
    # def management_unit_id_change(self):
    #     if self.management_unit_id:
    #         partner_s = self.env['res.partner']. \
    #        search([('management_unit_id', '=', self.management_unit_id)])
    #         self.partner_id = partner_s and partner_s[0] or False

    @api.multi
    def get_domain(self):
        domain = []
        state_id = self.state_id.id
        district_id = self.district_id.id
        ward_id = self.ward_id.id
        management_unit_id = self.management_unit_id.id
        day_staying = self.day_staying

        if state_id:
            domain.append(
                ('state_id', '=', state_id)
            )
        if district_id:
            domain.append(
                ('district_id', '=', district_id)
            )
        if ward_id:
            domain.append(
                ('ward_id', '=', ward_id)
            )
        if management_unit_id:
            domain.append(
                ('partner_id', '=', management_unit_id)
            )
        if day_staying:
            domain.append(
                ('day_staying', '>=', day_staying)
            )

        return domain

    @api.multi
    def view_report(self):
        action = self.env.ref(
            'bms_guest_stay_report.'
            'action_report_guest_staying_numbers_pivot_view')
        action_view = action.read()
        domain = self.get_domain()
        action_view[0]['domain'] = domain
        return action_view[0]

    @api.multi
    def get_accommodation_querry(self):
        state_id_where_clause = ""
        if self.state_id:
            state_id_where_clause = """ d.state_id = {}
                """.format(self.state_id.id)

        district_id_where_clause = ""
        if self.district_id:
            district_id_where_clause = """ and d.district_id = {}
                """.format(self.district_id.id)

        ward_id_where_clause = ""
        if self.ward_id:
            ward_id_where_clause = """and d.ward_id = {}
                """.format(self.ward_id.id)

        management_unit_id_where_clause = ""
        if self.management_unit_id:
            management_unit_id_where_clause = """ and d.partner_id = {}
                """.format(self.management_unit_id.id)

        day_staying_where_clause = ""
        if self.day_staying:
            day_staying_where_clause = """ and d.day_staying >= {}
                """.format(self.day_staying)

        querry = """
                    with d as(
                    select
                        g.id as id,
                        g.name as name,
                        g.day_number as day_number,
                        g.from_date,
                        now()::date -from_date::date + 1 as day_staying,
                        rs.id as state_id,
                        rd.id as district_id,
                        rw.id as ward_id,
                        coalesce(a.street || ', ' , '') ||
                        coalesce(arw.name || ', ' , '') ||
                        coalesce(ard.name|| ', ', '')||
                        coalesce(ars.name, '') as accommodation_address,
                        a.partner_id as partner_id,
                        a.id as accommodation_id,
                        a.name as accommodation_name,
                        a.mobile as phone
                    from bms_guest_stay as g
                        left join bms_accommodation
                        as a on g.accommodation_id = a.id
                        left join res_partner
                        as p on p.id = a.partner_id
                        left join res_country
                        as rc on rc.id = p.country_id
                        left join res_country_state
                        as rs on rs.id = p.state_id
                        left join res_country_district
                        as rd on rd.id = p.district_id
                        left join res_country_ward
                        as rw on rw.id = p.ward_id
                        left join res_country_state
                        as ars on ars.id = a.state_id
                        left join res_country_district
                        as ard on ard.id = a.district_id
                        left join res_country_ward
                        as arw on arw.id = a.ward_id
                        where g.day_number != -1
                    )
                    select accommodation_id as id,
                        accommodation_name,
                        state_id,
                        district_id,
                        ward_id,
                        accommodation_address,
                        partner_id,
                        phone,
                        count(id) as number_staying
                    from d
                    where {} {} {} {} {}
                    group by accommodation_id,
                        accommodation_name,
                        state_id,
                        district_id,
                        ward_id,
                        accommodation_address,
                        partner_id,
                        phone
                """.format(state_id_where_clause,
                           district_id_where_clause, ward_id_where_clause,
                           management_unit_id_where_clause,
                           day_staying_where_clause)
        return querry

    @api.multi
    def get_datas(self):
        querry = self.get_accommodation_querry()
        self.env.cr.execute(querry)
        querry_result = self.env.cr.fetchall()
        datas = []

        for row in querry_result:
            accommodation_id = row[0]
            accommodation_name = row[1]
            state_id = row[2] or _('Un define')
            district_id = row[3] or _('Un define')
            ward_id = row[4] or _('Un define')
            accommodation_address = row[5]
            partner_id = row[6]
            phone = row[7] or _('Un define')
            number_staying = row[8]

            datas.append(
                {
                    'accommodation_id': accommodation_id,
                    'accommodation_name': accommodation_name,
                    'state_id': state_id,
                    'district_id': district_id,
                    'ward_id': ward_id,
                    'accommodation_address': accommodation_address,
                    'partner_id': partner_id,
                    'phone': phone,
                    'number_staying': number_staying,
                }
            )

        return datas

    @api.multi
    def export_report(self):
        datas = {'ids': self.ids}
        datas['model'] = 'wizard.report.guest.staying.numbers'
        datas['form'] = self.read()[0]
        for field in datas['form'].keys():
            if isinstance(datas['form'][field], tuple):
                datas['form'][field] = datas['form'][field][0]

        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'bms_guest_stay.guest_staying_numbers_xls.xlsx',
            'datas': datas,
            'name': _('Guest Staying Number')
        }
