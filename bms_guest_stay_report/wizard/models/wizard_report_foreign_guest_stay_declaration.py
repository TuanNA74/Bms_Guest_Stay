# -*- coding: utf-8 -*-
from odoo import fields, models, api, _
import datetime
from dateutil.relativedelta import relativedelta


class WizardReportForeignersGuestStayDeclaration(models.TransientModel):
    _name = 'wizard.report.foreign.guest.stay.declaration'
    _description = 'Wizard report foreign guest stay declaration'
    _rec_name = 'id'
    _report_model_name = 'bms.guest.stay'

    def default_from_date(self):
        date_now = datetime.datetime.now()
        date_now = date_now.strftime('%Y-%m') + '-01'
        return date_now

    def default_to_date(self):
        date_now = datetime.datetime.now() + relativedelta(months=1)
        date_now = date_now.strftime('%Y-%m') + '-01'
        date_now = datetime.datetime.strptime(date_now, '%Y-%m-%d')
        date_now = date_now + relativedelta(days=-1)
        date_now = date_now.strftime('%Y-%m-%d')
        return date_now

    def _state_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return self.env.user.state_id.id
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.state_id.id
        return False

    def _district_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.district_id.id
        return False

    def _ward_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.ward_id.id
        return False

    def _management_unit_id_default(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official')
        if is_super_manager:
            return False
        is_management_unit = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_principal_official')
        if is_management_unit:
            return self.env.user.partner_id.id
        return False

    def _is_super_manager_default(self):
        res = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        return res

    state_id = fields.Many2one('res.country.state', string='State',
                               default=_state_id_default)
    district_id = fields.Many2one('res.country.district',
                                  string='District',
                                  default=_district_id_default)
    ward_id = fields.Many2one('res.country.ward', string='Ward',
                              default=_ward_id_default)
    management_unit_id = fields.Many2one('res.partner',
                                         string='Management unit',
                                         default=_management_unit_id_default)

    from_date = fields.Date(required=True, default=default_from_date)
    to_date = fields.Date(required=True, default=default_to_date)

    type = fields.Selection([('default', _('Default')),
                             ('default2', _('Default with accommodation')),
                             ('5_columns',
                              _('divides 5 columns of papers'))],
                            default='default',
                            required=True)
    template = fields.Selection([('1', '1'),
                                 ('2', '2'),
                                 ('3', '3')], required=True,
                                default='1')

    is_super_manager = fields.Boolean(compute='_compute_is_super_manager',
                                      default=_is_super_manager_default)

    @api.model
    def _compute_is_super_manager(self):
        is_super_manager = self.env.user.has_group(
            'bms_guest_stay.group_e_guest_senior_official') or False
        self.is_super_manager = is_super_manager

    @api.multi
    def management_unit_onchange(
            self, state_id, district_id, ward_id, context={}):

        res = {
            'value': {}
        }
        if state_id:
            res['domain'] = {
                'management_unit_id': [('state_id', '=', state_id),
                                       ('is_management_unit', '=', True)]
            }

        if district_id:
            res['domain'] = {
                'management_unit_id':
                    [('district_id', '=', district_id),
                     ('is_management_unit', '=', True)]
            }

        if ward_id:
            res['domain'] = {
                'management_unit_id': [('ward_id', '=', ward_id),
                                       ('is_management_unit', '=', True)]
            }

        if context.get('change_state', False):
            res['value']['district_id'] = False
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_district', False):
            res['value']['ward_id'] = False
            res['value']['management_unit_id'] = False

        if context.get('change_ward', False):
            res['value']['management_unit_id'] = False

        return res

    def get_data(self):
        domain = self.get_domain()
        report_s = self.env[
            'report.foreign.guest.stay.declaration'
        ].search_read(domain, [])
        accommodation_ids = []
        for report in report_s:
            if report['accommodation_id'][0] not in accommodation_ids:
                accommodation_ids.append(report['accommodation_id'][0])
        accommodation_s = self.env['bms.accommodation'].read(
            accommodation_ids,
            ['country_id',
             'state_id',
             'district_id',
             'ward_id'])
        accommodation_dict = {}
        for accommodation in accommodation_s:
            ward = \
                accommodation['ward_id'] \
                and accommodation['ward_id'][1] + ' - ' or ''
            district = \
                accommodation['district_id'] \
                and accommodation['district_id'][1] + ' - ' or ''
            state = \
                accommodation['state_id'] \
                and accommodation['state_id'][1] or ''
            street = ward + district + state
            accommodation_dict[accommodation['id']] = street

        return report_s, accommodation_dict

    @api.multi
    def export_report(self):
        datas = {'ids': self.ids}
        datas['model'] = 'wizard.report.foreign.guest.stay.declaration'
        datas['form'] = self.read()[0]

        return {
            'type': 'ir.actions.report.xml',
            'report_name':
                'bms_guest_stay.foreign_guest_stay_declaration_xls.xlsx',
            'datas': datas,
            'name': _('Foreign guest stay declaration')
        }

    @api.multi
    def get_domain(self):
        domain = []
        state_id = self.state_id.id
        district_id = self.district_id.id
        ward_id = self.ward_id.id
        management_unit_id = self.management_unit_id.id
        from_date = self.from_date
        to_date = self.to_date
        if state_id:
            domain.append(
                ('state_id', '=', state_id)
            )
        if district_id:
            domain.append(
                ('district_id', '=', district_id)
            )
        if ward_id:
            domain.append(
                ('ward_id', '=', ward_id)
            )
        if management_unit_id:
            domain.append(
                ('partner_id', '=', management_unit_id)
            )

        domain.append(
            ('from_date', '>=', from_date),
        )
        domain.append(
            ('from_date', '<=', to_date),
        )
        return domain

    @api.multi
    def view_report(self):
        action = self.env.ref(
            'bms_guest_stay_report.'
            'action_report_foreign_guest_stay_declaration_view')
        action_view = action.read()
        view_id = 'bms_guest_stay_report'
        '.report_foreign_guest_stay_declaration_tree_view' + self.template
        domain = self.get_domain()
        action_view[0]['domain'] = domain
        action_view[0]['context'] = {'form_view_ref': view_id}
        return action_view[0]

    def tr(self, field_name):
        uid = self.env.uid
        user = self.env['res.users'].browse(uid)
        lang = user.partner_id.lang

        name = self._report_model_name + ',' + field_name

        trans = self.env['ir.translation'].search_read(
            [('lang', '=', lang),
             ('type', '=', 'selection'),
             ('name', '=', name)],
            ['source', 'value']
        )

        res = dict((tran['source'], tran['value']) for tran in trans)
        return res

    @api.model
    def get_tr_selection(self, field_name):
        model_name = 'bms.guest.stay'
        uid = self.env.uid
        user = self.env['res.users'].browse(uid)
        model_bject = self.env[model_name]. \
            with_context(lang=user.partner_id.lang)
        field = model_bject._fields[field_name]
        selection = field.selection

        tr_dict = self.tr(field_name)

        selection_tr_dict = \
            dict((s[0], tr_dict.get(s[1], s[1])) for s in selection)
        return selection_tr_dict
